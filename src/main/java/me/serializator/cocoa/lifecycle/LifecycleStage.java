package me.serializator.cocoa.lifecycle;

import me.serializator.cocoa.CocoaMinigame;
import org.bukkit.event.Listener;

/**
 * A stage in the lifecycle of a minigame.
 * <br /><br />
 * A stage is also a {@link Listener} and all its event handlers (methods with the {@link org.bukkit.event.EventHandler} annotation)
 * are registered once the stage starts and unregistered once the stage stops.
 *
 * @param <P> the {@link CocoaMinigame} that has control over the lifecycle
 */
public abstract class LifecycleStage<P extends CocoaMinigame<P>> implements Listener {
    protected final P plugin;

    public LifecycleStage(P plugin) {
        this.plugin = plugin;
    }

    public void start() {
        // overridden by the subclass if necessary
    }

    public void stop() {
        // overridden by the subclass if necessary
    }

    /**
     * Convenience method to engage a stage from within this stage.
     *
     * @param stage the stage to engage.
     */
    public final void engageStage(LifecycleStage<P> stage) {
        plugin.getLifecycle().engageStage(stage);
    }
}
