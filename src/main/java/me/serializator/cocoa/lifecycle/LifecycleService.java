package me.serializator.cocoa.lifecycle;

import me.serializator.cocoa.CocoaMinigame;
import org.bukkit.event.HandlerList;

/**
 * Service that controls the lifecycle of a minigame.
 *
 * @param <P> the {@link CocoaMinigame} that has control over the lifecycle
 */
public class LifecycleService<P extends CocoaMinigame<P>> {
    private final P plugin;
    private volatile LifecycleStage<P> stage;

    /**
     * Constructs a new {@link LifecycleService} instance.
     *
     * @param stage the initial {@link LifecycleStage}
     */
    public LifecycleService(P plugin, LifecycleStage<P> stage) {
        this.plugin = plugin;
        engageStage(stage);
    }

    /**
     * Engage a {@link LifecycleStage} by registering its event handlers and invoking
     * {@link LifecycleStage#start()}
     *
     * This will also cancel any stage that is currently engaged by invoking
     * {@link LifecycleStage#stop()} and unregistering its event handlers.
     *
     * @param stage the stage to engage
     */
    public final synchronized void engageStage(LifecycleStage<P> stage) {
        if(this.stage != null) {
            this.stage.stop();
            HandlerList.unregisterAll(this.stage);
        }

        plugin.getServer().getPluginManager().registerEvents(stage, plugin);
        stage.start();
        this.stage = stage;
    }

    /**
     * Cancel the current stage by invoking {@link LifecycleStage#stop()}
     * and unregistering its event handlers.
     * <br /><br />
     * <strong>This method should only be invoked by {@link CocoaMinigame#onDisable()} to perform cleanup.</strong>
     */
    public final synchronized void cancelStage() {
        if(stage == null) {
            return;
        }

        stage.stop();
        HandlerList.unregisterAll(stage);
        stage = null;
    }

    /**
     * Get the stage that is currently engaged.
     *
     * @return stage that is currently engaged or null if there is no stage.
     */
    public final LifecycleStage<P> getStage() {
        return stage;
    }
}
