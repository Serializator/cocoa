package me.serializator.cocoa;

import me.serializator.cocoa.lifecycle.LifecycleService;
import me.serializator.cocoa.lifecycle.LifecycleStage;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Represents a Cocoa minigame.
 */
public abstract class CocoaMinigame<P extends CocoaMinigame<P>> extends JavaPlugin {
    private LifecycleService<P> lifecycle;

    @Override
    public final void onEnable() {
        lifecycle = new LifecycleService<P>((P) this, createInitialLifecycleStage());
        onCocoaEnable();
    }

    /**
     * Invoked after the bootstrapping of Cocoa is done so that the minigame using Cocoa has access to all
     * of Cocoa's features in the enable process.
     */
    protected abstract void onCocoaEnable();

    @Override
    public final void onDisable() {
        onCocoaDisable();
        lifecycle.cancelStage();
    }

    /**
     * Invoked before Cocoa is disabled itself so that the minigame using Cocoa still has access to all
     * of Cocoa's features in the disable process.
     */
    protected abstract void onCocoaDisable();

    protected abstract LifecycleStage<P> createInitialLifecycleStage();

    public final LifecycleService<P> getLifecycle() {
        return lifecycle;
    }
}
